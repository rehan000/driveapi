const GoogleDrive = require("./GoogleDrive.js");

class DownloadGoogleDriveFiles 
{
    async init(baseDirPath, startDate, endDate)
    {
        let googleDrive = new GoogleDrive;

        let basePathFolderId = await googleDrive.getBaseDirFolderIdFromFolderPath(baseDirPath);

        let allFolders = await googleDrive.getAllFoldersFromFolderIdRecursively(basePathFolderId);

        let allFiles = await googleDrive.getAllFilesFromFolders(allFolders);

        let filteredFiles = googleDrive.getFilesFromDateRange(allFiles, startDate, endDate);

        await googleDrive.downloadFiles(filteredFiles);
        
        process.exit();
    }
}

let baseDirPath = 'Folder1/Folder2/Invoices';
let startDate = "2021/07/10";
let endDate = "2021/09/13";

let downloadGoogleDriveFiles = new DownloadGoogleDriveFiles();

(async() => {
    await downloadGoogleDriveFiles.init(baseDirPath, startDate, endDate);
})();
